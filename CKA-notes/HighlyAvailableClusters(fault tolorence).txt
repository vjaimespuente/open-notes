HIGHLY AVAILABLE CLUSTER (fault tolerance)
	There are a few ways to create highly available clusters. 
	 	Keeping replicas of your application on different nodes
		Adding additional nodes 
	There is also a way to keep protect against failure of the kube system components by replicating them as well. 
	* To me this seems like an extra layer of protection. We have already created a process that will protect us from node crashes, this will protect us from individiual K8 component crashes. 


	*Process >> : 

	View the pods in the default namespace with a custom view:
		**
		kubectl get pods -o custom-columns=POD:metadata.name,NODE:spec.nodeName --sort-by spec.nodeName -n kube-system 
		**
	This command allows you to choose custom columns and specify what you acn see fromt he kubectl  get pods command, especially if you could get it on a single line. 

	Each component of the control plane, the master server, can be replicated.

	However, some components may need to stay in a standby state in order to eliminate conflict with the replicated components. 

	Running multiple instances in tandem would result in them constantly competing to change the state of the cluster and possibly creating duplicate resource is this would not be good. Instead only one should be active at a time.
	
	How do we set some to inactive and one to active?

		 Well, it's controlled by the leader elect option.
 		A leader is selected as theat active component. The setting is set to true by default, which means that they all will be aware of the leader and also, when they themselves become the elected leader.
		
		The leader election mechanism is used by creating an endpoint resource, and you can see that endpoint resource in the Cube schedule or Yamil.
		
		** 
			kubectl get endpoints kube-scheduler -n kube-system -o yaml
		**

		In the yaml, you'll see there's an annotation that says this particular instance is the current leader, and it contains a field called Holder Identity, which holds the name of the current leader, which is one seat, which is the master server in this case, once becoming the leader, It must periodically update the resource, and that happens every two seconds by default.

Replcatin etcd 

	The etcd instances can be in a stacked or external, etcd topology, 

	a stacked at etcd topology is where each control plane node creates a local etcd member. And this etcd member communicates only with the kube-apiserver of this node,
	An external etcd topology is when etcd is external to the Kubernetes cluster.
	
	Etcd uses a raft consensus algorithm which requires a majority in order to progress to the next state. 
	
	There needs to be more than half of the nodes taking part in that state change. Therefore, in order to have a majority, you must have an odd number of that etcd instances. 
	This is because, let's say, if they're to the cluster can't transition to a new state because no majority exists. So having two etcd instances is worse than having one
	even in the largest of environments. Most of the time, you won't need more than seven at etcd instances

	 You download the etcd binaries extract and move the binaries to user local bin you create two directories, etc./etcd/ and var/lib/etcd.  
    
    You create the systemd unit file for etcd and you enable and start the TV service. 
    
    Once you do this, you would start to install the other Kubernetes components and after completing all of this, as well as keeping the appropriate number of pod replicas and nose for your application to run on, you will have your entire cluster highly available.

	