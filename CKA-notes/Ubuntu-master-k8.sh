apt update 

echo updated 

# get docker gpg key 
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

echo lilne 8 : curled docker gpg key 

# add docker repo 
sudo add-apt-repository    "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
echo line 12 : added docker repo 

# get the k8 gpg key
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -

echo line 19 : curled k8 gpg key

# Add the Kubernetes repository:
cat << EOF | sudo tee /etc/apt/sources.list.d/kubernetes.list
deb https://apt.kubernetes.io/ kubernetes-xenial main
EOF

echo line 26 : added k8 repo

# Update your packages:
sudo apt-get update

echo line 32 : updated 

# Install Docker, kubelet, kubeadm, and kubectl:
sudo apt-get install -y docker-ce=5:19.03.12~3-0~ubuntu-bionic kubelet=1.17.8-00 kubeadm=1.17.8-00 kubectl=1.17.8-00

echo line 38 :  Installed Docker, kubelet, kubeadm, and kubectl

# Add the iptables rule to sysctl.conf:
echo "net.bridge.bridge-nf-call-iptables=1" | sudo tee -a /etc/sysctl.conf

echo line 44 : Added the iptables rule to sysctl.conf

# Enable iptables immediately:
sudo sysctl -p

echo line 46 : Enabled iptables

# Initialize the cluster
sudo kubeadm init --pod-network-cidr=10.244.0.0/16

echo initilized the cluster JOIN COMMAND **********

# Set up local kubeconfig
mkdir -p $HOME/.kube

sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config

sudo chown $(id -u):$(id -g) $HOME/.kube/config

echo Set up kubeconfig

# Apply Calico CNI network overlay
kubectl apply -f https://docs.projectcalico.org/v3.14/manifests/calico.yaml

echo Applied Calico CNI network overlay

echo READY TO RUN JOIN COMMAND ON SLAVES 



